#!/bin/sh

source "$(dirname $(readlink -f $0))/color.sh"

patchFolder="./etc/patch/"

create_vendor_patch() {
    i=0

    for new in $(find ./vendor -name "*.new")
        do
            new="$new"

            old="$(sed -r 's/(.+)\.new$/\1/g' <<<$new)"

            echo "Processing $old"

            patchName="$patchFolder$(sed -r 's/\//_/g' <<<$old | sed -r 's/\._//g').patch"

            # simple without A/B
            diff -u $old $new > $patchName

            # A/B variant
        #    diff="$(diff -u $old $new)"
        #
        #    a="$(sed -r 's/^\./a/g' <<<$old)"
        #    b="$(sed -r 's/^\./b/g' <<<$old)"
        #
        #    timestamp=$(date +%s)
        #    head="diff --git $a $b\n--- $a\tdate($timestamp)\n--- $b\tdate($timestamp)\n"
        #
        #    echo "	Creating patch [$patchName]"
        #    sed '1,2d' <<<$diff | sed "1s;^;$head;" > $patchName

            echo "	Removing file [$new]"
            rm $new

            echo "	Applying patch [$patchName]"
            git apply $patchName

            ((i+=1))
        done
    echo "Done, processed files: $i"
}

apply_patch() {
    for patch in $(find $patchFolder -name "*.patch");
        do
            if git apply $patch 2> /dev/null
                then
                    printf "Patch ${GREEN}SUCCESS${NC}: ${PURPLE}$patch${NC}\n"
                else
                    printf "Patch ${RED}FAIL${NC}: ${PURPLE}$patch${NC}\n"
            fi
        done
}

if [ -z "$1" ]
    then
      echo "Function name needed" >&2
      exit 1
fi

if declare -f "$1" > /dev/null
    then
        # call function
        "$@"
    else
        echo "'$1' is not a known function name" >&2
        exit 1
fi
