
# Reset
NC=\033[0m

# Regular Colors
BLACK=\033[0;30m
RED=\033[0;31m
GREEN=\033[0;32m
YELLOW=\033[0;33m
BLUE=\033[0;34m
PURPLE=\033[0;35m
CYAN=\033[0;36m
WHITE=\033[0;37m

# Bold
BBLACK=\033[1;30m
BRED=\033[1;31m
BGREEN=\033[1;32m
BYELLOW=\033[1;33m
BBLUE=\033[1;34m
BPURPLE=\033[1;35m
BCYAN=\033[1;36m
BWHITE=\033[1;37m

# Underline
UBLACK=\033[4;30m
URED=\033[4;31m
UGREEN=\033[4;32m
UYELLOW=\033[4;33m
UBLUE=\033[4;34m
UPURPLE=\033[4;35m
UCYAN=\033[4;36m
UWHITE=\033[4;37m

# Background
ON_BLACK=\033[40m
ON_RED=\033[41m
ON_GREEN=\033[42m
ON_YELLOW=\033[43m
ON_BLUE=\033[44m
ON_PURPLE=\033[45m
ON_CYAN=\033[46m
ON_WHITE=\033[47m

# High Intensity
IBLACK=\033[0;90m
IRED=\033[0;91m
IGREEN=\033[0;92m
IYELLOW=\033[0;93m
IBLUE=\033[0;94m
IPURPLE=\033[0;95m
ICYAN=\033[0;96m
IWHITE=\033[0;97m

TARGETS_TEST=tests/Unit tests/Functional



## COMPOSER ------------------------------------------------------------------------------------------------------------

.PHONY: lockfix
## Fix git* urls in composer.lock (there is no settings yet v1.9.1)
lockfix:
	if [ "$(shell uname)" == "Linux" ]; then sed -i -E 's/git@gitlab([\w\d\.]+):([\w\d\.\/]+)/https:\/\/gitlab\1\/\2/g' composer.lock; fi

.PHONY: outdated
## Show outdated composer packages
outdated:
	composer outdated -D

## PHP CODE STYLE CHECK / FIX ------------------------------------------------------------------------------------------

PHP_CS_SETTINGS=--standard=./etc/ruleset.xml --extensions=php

.PHONY: cs
## Code style (dry run)
cs:
	vendor/bin/phpcs $(PHP_CS_SETTINGS) -sp src $(TARGETS_TEST) --parallel=8

.PHONY: cs-fix
## Fix code style
cs-fix:
	vendor/bin/phpcbf $(PHP_CS_SETTINGS) -sp src $(TARGETS_TEST)

## PHP STAN ------------------------------------------------------------------------------------------------------------

.PHONY: stan-src
## Run PHPStan on /src folder
stan-src:
	php -d memory_limit=256M vendor/bin/phpstan analyse src -c etc/phpstan.neon --level 8

.PHONY: stan-tests
## Run PHPStan on /tests folder
stan-tests:
	php -d memory_limit=256M vendor/bin/phpstan analyse $(TARGETS_TEST) -c etc/phpstan_tests.neon --level 8

.PHONY: stan
## Run PHPStan
stan: stan-src stan-tests

.PHONY: stancc
## Clear PHPStan cache
stancc:
	php vendor/bin/phpstan clear-result-cache

## TEST ----------------------------------------------------------------------------------------------------------------

.PHONY: build-tests
## Build Codeception tests
build-tests:
	php vendor/bin/codecept build

TEST_TEMPLATE=XDEBUG_MODE=coverage php vendor/bin/codecept run
TEST_TEMPLATE_OPTIONS=--coverage-cobertura --verbose

.PHONY: unit
## Run unit tests
unit:
	php $(TEST_TEMPLATE) unit $(TEST_TEMPLATE_OPTIONS)
	mv tests/_output/cobertura.xml tests/_output/cobertura.unit.xml

.PHONY: functional
## Run functional tests
functional:
	php $(TEST_TEMPLATE) functional $(TEST_TEMPLATE_OPTIONS)
	mv tests/_output/cobertura.xml tests/_output/cobertura.functional.xml

.PHONY: test
test: ./bin/build/test_targets.txt

## CI ------------------------------------------------------------------------------------------------------------------

.PHONY: ci
## Run all CI commands
ci: lockfix cs stan test

.PHONY: patch-vendor
## Apply *.patch files for ./vendor folder
patch-vendor:
	sh ./bin/vendor_patcher.sh apply_patch

# copy a vendor file to file.new
# make desired changes in file.new
.PHONY: generate-vendor-patch
## Generate *.patch files from vendor changes
generate-vendor-patch:
	sh ./bin/vendor_patcher.sh create_vendor_patch

## LOCAL SERVER --------------------------------------------------------------------------------------------------------
.PHONY: server
server:
	php -S localhost:8000 public/index.php

# Documentation
.DEFAULT_GOAL := help

TARGET_MAX_CHAR_NUM=22
.SILENT:
.PHONY: help
help:
	@printf "Available commands:\n";
	@awk '{ \
		if ($$0 ~ /^.PHONY: [a-zA-Z\-\\_0-9]+$$/) { \
			helpCommand = substr($$0, index($$0, ":") + 2); \
			if (helpMessage) { \
				cnt=cnt+1; \
				if (cnt%2 == 0) { bg=bgone; } else { bg=bgtwo; } \
				printf " ${BRED}"bg"%-"width"s${NC}${BBLUE}"bg"%s${NC}\n", \
					helpCommand, helpMessage; \
				helpMessage = ""; \
			} \
		} else if ($$0 ~ /^[a-zA-Z\-\\_0-9.]+:/) { \
			helpCommand = substr($$0, 0, index($$0, ":")-1); \
			if (helpMessage) { \
				cnt=cnt+1; \
				if (cnt%2 == 0) { bg=bgone; } else { bg=bgtwo; } \
				printf " ${BRED}"bg"%-"width"s${NC}${BBLUE}"bg"%s${NC}\n", \
					helpCommand, helpMessage; \
				helpMessage = ""; \
			} \
		} else if ($$0 ~ /^##/) { \
			if (helpMessage) { \
				helpMessage = helpMessage "\n"  substr($$0, 3); \
			} else { \
				helpMessage = substr($$0, 3); \
			} \
		} else { \
			if (helpMessage) { \
				print helpMessage; \
				cnt=1; \
			} \
			helpMessage = ""; \
		} \
	}' \
	cnt=1 \
	bgone="\033[48;2;63;50;63m" \
	bgtwo="" \
	width="$$(($$(grep -o '^[a-zA-Z0-9_-]\+:' $(MAKEFILE_LIST) | wc -L)-8))" \
	$(MAKEFILE_LIST)
