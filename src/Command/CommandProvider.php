<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Command;

interface CommandProvider
{
    /**
     * @throws CommandNotFound
     */
    public function getCommandByMethodName(string $methodName): Command;
}
