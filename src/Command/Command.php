<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Command;

use JsonRpc\Generator\Response\Response;

interface Command
{
    public function execute(array $params): Response;
}
