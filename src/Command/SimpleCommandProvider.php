<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Command;

final readonly class SimpleCommandProvider implements CommandProvider
{
    /**
     * @param array<class-string<Command>> $commands
     */
    public function __construct(
        private array $commands,
    ) {}


    /**
     * @throws CommandNotFound
     */
    public function getCommandByMethodName(string $methodName): Command
    {
        $commandName = $this->commands[$methodName] ?? throw new CommandNotFound;

        return new $commandName;
    }
}
