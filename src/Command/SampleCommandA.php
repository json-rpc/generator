<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Command;

use JsonRpc\Generator\Response\Response;
use JsonRpc\Generator\Response\SuccessResponse;

final readonly class SampleCommandA implements Command
{
    public function execute(array $params): Response
    {
        return SuccessResponse::with(
            [
                'command' => 'sample command A',
            ],
        );
    }
}
