<?php

declare(strict_types=1);

namespace JsonRpc\Generator;

use ColinODell\Json5\SyntaxError;
use JsonRpc\Generator\Command\CommandNotFound;
use JsonRpc\Generator\Command\CommandProvider;
use JsonRpc\Generator\Request\InvalidMethodParams;
use JsonRpc\Generator\Request\InvalidRequestFormat;
use JsonRpc\Generator\Response\ErrorCode;
use JsonRpc\Generator\Response\ErrorResponse;
use JsonRpc\Generator\Response\Response;
use JsonRpc\Generator\Response\ResponseBuilder;
use function Safe\file_get_contents;
use function Safe\json_encode;

final readonly class App
{
    public function __construct(
        private ResponseBuilder $responseBuilder,
        private CommandProvider $commandProvider,
    ) {}


    public function run(): void
    {
        $response = $this->handleRequest();

        echo json_encode($response);
    }


    public function handleRequest(): array
    {
        $body = file_get_contents('php://input');

        # todo json
        //if (\json_validate($body)) {
        //    return $this->responseBuilder->build(
        //            ErrorResponse::with(
        //            ErrorCode::PARSE_ERROR,
        //            'Invalid JSON',
        //            ),
        //            'error',
        //        );
        //}
        //
        //$decoded = json_decode($body, true);

        # todo json5
        try {
            $decoded = \json5_decode($body, true);
        } catch (SyntaxError) {
            return $this->responseBuilder->build(
                ErrorResponse::with(
                    ErrorCode::PARSE_ERROR,
                    'Parse error.',
                ),
                'error',
            );
        }

        if(\array_is_list($decoded)) {
            $response = [];

            foreach ($decoded as $singleRequest) {
                $response[] = $this->buildSingleRequest($singleRequest);
            }
        } else {
            $response = $this->buildSingleRequest($decoded);
        }

        return $response;
    }


    private function buildSingleRequest(array $singleRequest): array
    {
        try {
            $response = $this->handleSingleRequest($singleRequest);
        } catch (\Exception $e) {
            #todo log to sentry

            $response = ErrorResponse::with(
                ErrorCode::INTERNAL_ERROR,
                'Internal error.',
            );
        }

        return $this->responseBuilder->build(
            $response,
            $singleRequest['id'] ?? null,
        );
    }


    private function handleSingleRequest(array $singleRequest): Response
    {
        $jsonRpcVersion = $singleRequest['jsonrpc'] ?? null;
        $method = $singleRequest['method'] ?? null;
        $params = $singleRequest['params'] ?? null;

        try {
            $this->assertRpcFormat(
                $jsonRpcVersion,
                $method,
            );
        } catch (InvalidRequestFormat) {
            return ErrorResponse::with(
                ErrorCode::INVALID_REQUEST,
                'Invalid json-rpc request.',
            );
        }

        try {
            $command = $this->commandProvider->getCommandByMethodName($method);
        } catch (CommandNotFound) {
            return ErrorResponse::with(
                ErrorCode::METHOD_NOT_FOUND,
                'Method not found.',
            );
        }

        try {
            $this->assertParams($params);
        } catch (InvalidMethodParams) {
            return ErrorResponse::with(
                ErrorCode::INVALID_PARAMS,
                'Invalid method parameters.',
            );
        }

        return $command->execute($params);
    }


    /**
     * @throws InvalidRequestFormat
     */
    private function assertRpcFormat(
        ?string $jsonRpcVersion,
        ?string $method,
    ): void
    {
        if ($jsonRpcVersion !== '3.0'
            || $method === null
            || $method === ''
        ) {
            throw new InvalidRequestFormat;
        }
    }


    private function assertParams(array $params): void
    {
        #todo assert params schema
        throw new InvalidMethodParams;
    }
}
