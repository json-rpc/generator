<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Request;

final class InvalidMethodParams extends \RuntimeException
{}
