<?php

declare(strict_types=1);

namespace JsonRpc\Generator;

use JsonRpc\Generator\Command\SampleCommandA;
use JsonRpc\Generator\Command\SampleCommandB;
use JsonRpc\Generator\Command\SimpleCommandProvider;
use JsonRpc\Generator\Response\ResponseBuilder;

final readonly class Kernel
{
    public function run(): void
    {
        $responseBuilder = new ResponseBuilder;

        $commandProvider = new SimpleCommandProvider(
            [
                'sample.commandA' => SampleCommandA::class,
                'sample.commandB' => SampleCommandB::class,
            ],
        );

        $app = new App(
            $responseBuilder,
            $commandProvider,
        );

        $app->run();
    }
}
