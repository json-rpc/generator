<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Response;

final readonly class ErrorResponse implements Response
{
    public static function with(
        ErrorCode $code,
        string $message,
    ): self
    {
        return new self($code, $message);
    }


    public function __construct(
        public ErrorCode $code,
        public string $message,
    ) {}
}
