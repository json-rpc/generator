<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Response;

enum ErrorCode: int
{
    case PARSE_ERROR = -32700; // Invalid JSON was received by the server.
    case INVALID_REQUEST = -32600; // The JSON sent is not a valid Request object.
    case METHOD_NOT_FOUND = -32601; // The method does not exist / is not available.
    case INVALID_PARAMS = -32602; // Invalid method parameters.
    case INTERNAL_ERROR = -32603; // Internal JSON-RPC error.

    case GENERAL_ERROR = 1;
}
