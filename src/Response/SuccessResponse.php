<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Response;

final readonly class SuccessResponse implements Response
{
    public static function with(
        array $result,
    ): self
    {
        return new self($result);
    }


    public function __construct(
        public array $result,
    ) {}
}
