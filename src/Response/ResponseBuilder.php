<?php

declare(strict_types=1);

namespace JsonRpc\Generator\Response;

use Safe\DateTimeImmutable;

final readonly class ResponseBuilder
{
    public function build(
        Response $response,
        int|string|null $id,
    ): array
    {
        $responseData = [
            'jsonrpc' => '3.0',
            'id' => $id,
        ];

        if ($response instanceof ErrorResponse) {
            $responseData['error'] = [
                'code' => $response->code,
                'message' => $response->message,
            ];
        }

        if ($response instanceof SuccessResponse) {
            $responseData['result'] = $response->result;
        }

        $responseData['time'] = (new DateTimeImmutable)->format(DATE_ATOM);

        return $responseData;
    }
}
