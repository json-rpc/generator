<?php

declare(strict_types=1);

umask(002);

require __DIR__ . '/../vendor/autoload.php';

$kernel = new \JsonRpc\Generator\Kernel();

$kernel->run();
